#!/usr/bin/env bash
set -e

[ -z "$1" ] && echo "Usage: ./build_data.sh DATA_BUILD_PATH" && exit -1

sed "s@#DATA_BUILD#@mkdir \$\{SINGULARITY_ROOTFS\}/data \&\& cp -vr ${1}/* \$\{SINGULARITY_ROOTFS\}/data/ \&\& chmod -R a+rx \$\{SINGULARITY_ROOTFS\}/data/@g" Singularity > Singularity.data
sudo singularity build cgi_data.simg Singularity.data
sudo chown ${USER}: cgi_data.simg
rm Singularity.data

