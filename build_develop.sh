#!/usr/bin/env bash
set -e

sed "s#pip3 --no-cache-dir install /cgi.tar.gz#mkdir -p ${PWD} \&\& tar xvzf /cgi.tar.gz -C ${PWD} \&\& pip3 --no-cache-dir install -e ${PWD}#g" Singularity > Singularity.dev
sudo singularity build cgi_develop.simg Singularity.dev
sudo chown ${USER}: cgi_develop.simg
rm Singularity.dev
python setup.py sdist

