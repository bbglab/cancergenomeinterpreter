# Installing CGI in GEL

1. Transfer the datasets.tar.xz file and the cancergenomeinterpreter-1.0.0.tar.gz
2. Uncompress the datasets.tar.xz

   ```bash
   $ tar -xvf datasets.tar.xz
   ```

   It is important to keep track of where the data is uncompressed as the
   path needs to be used in different places.

3. Update the transvar configuration file with the path to dataset:

   ```bash
   $ sed -i.bkp "s@/data/@/path/to/datasets/@g" transvar.cfg
   ```

4. Install Cancer Genome Interpreter (we have tested it under Python 3.6)

   ```bash
   $ pip install cancergenomeinterpreter-1.0.0.tar.gz
   ```

5. For running CGI, 3 variables need to be exported:

   ```bash
   export CGI_DATA=/path/to/datasets
   export TRANSVAR_CFG=${CGI_DATA}/transvar.cfg
   export TRANSVAR_DOWNLOAD_DIR=${CGI_DATA}/transvar
   ```

6. CGI can be run from the command line:

   ```bash
   $ cgi -i input_file1 -i input_file2 -t COREAD
   ```


# Using CGI

CGI provides one high level command ``cgi`` and
four low level commands (``cgi-cna``, ``cgi-mut``, ``cgi-fus`` and ``cgi-prescription``)
that perform part of the tasks
done by the high level one.

When using the ``cgi`` command different files for mutations, CNA and
translocations can be provided by using ``-i`` multiple times; e.g.: 

```bash
$ cgi -i muts.tsv -i cnas.tsv
```

Input files need to be tab separated files where the first line
will be treated as header. Columns not mentioned below
will be ignored but included in the output.
An optional ``sample`` column can be included with the sample identifier
in all files.

**Mutations** can be stated as:
 
- *protein changes* following
  [HGVS format](http://www.hgvs.org/mutnomen/examplesAA.html)
  on the ``protein`` column
  
- *chromosomal changes* following
  [HGVS format](http://www.hgvs.org/mutnomen/recs-DNA.html)
  on the ``gdna`` column
  
- *chromosomal changes* following
  genomic tabular format with
  ``chr``, ``pos``, ``ref`` and ``alt`` columns
  
- *chromosomal changes* following
  [VCF format](http://www.1000genomes.org/wiki/Analysis/vcf4.0)
  
**Copy Number Alterations** require two columns:
``gene`` and ``cna``. Possible values in the later are ``amp`` and ``del``.
  
**Translocations** require a ``fus`` column
in which both partners are separated by two underscores ("__").

See https://www.cancergenomeinterpreter.org/formats for further details.

The ``-t`` option can be used to indicate the cancer type.
By default (if not provided or set to CANCER) refers to any cancer type.
Available cancer types are the same that can be found in the website
(https://www.cancergenomeinterpreter.org/analysis) or in the
datasets/cgi/cancertypes/cancer_types.tsv file
(see ID column).



# Instructions for Santi

1. Create pip environment

   $ module load python/3.6.7
   $ python -m venv /some/path/for/env
   $ source /some/path/for/env/bin/activate

2. Install CGI

   $ pip install /re_gecip/cancer_colorectal/cgi/cgicancergenomeinterpreter-1.0.0.tar.gz
   
3. Export the appropiate variables:

   export CGI_DATA=/re_gecip/cancer_colorectal/cgi/datasets
   export TRANSVAR_CFG=${CGI_DATA}/transvar.cfg
   export TRANSVAR_DOWNLOAD_DIR=${CGI_DATA}/transvar

4. Run test

   $ cgi -i /re_gecip/cancer_colorectal/cgi/input_cna.txt -i /re_gecip/cancer_colorectal/cgi/input_prot.txt -o test_output

