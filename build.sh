#!/usr/bin/env bash
set -e

sudo singularity build cgi.simg Singularity
sudo chown ${USER}: cgi.simg

