import os
import tempfile
import subprocess

from os import path


PROJECT_FOLDER = path.abspath(path.join(path.dirname(__file__), '..'))
COMMAND = "singularity run -B {}:/data {}/cgi.simg".format(os.environ['CGI_BUILD'], PROJECT_FOLDER)


def test_01_mut():
    
    with tempfile.TemporaryDirectory() as tmp:
        cmd = "{} -i {} -o {}".format(COMMAND, path.join(PROJECT_FOLDER, 'tests', '01_mut', 'input.tsv'), tmp)
        print(cmd)
        return_code = subprocess.call(cmd, shell=True)
        
    assert return_code == 0
    
    

