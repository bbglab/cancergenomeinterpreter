#!/bin/bash
source ~/.bashrc
source activate tcgi_develop 2>/dev/null
bg-ping -u "http://gencluster.prib.upf.edu:9090/api/status?id=dd68162522aa&host=172.20.16.27&port=8282&line=STA:Running"
tcgi -i /projects_bg/bg/bgcluster/tcgi/jobs/dd68162522aa/input01.tsv -o /projects_bg/bg/bgcluster/tcgi/jobs/dd68162522aa -t NSCLC 2>&1 | bg-tee -p "LOG:" -u "http://gencluster.prib.upf.edu:9090/api/status?id=dd68162522aa&host=172.20.16.27&port=8282&line="
err=${PIPESTATUS[0]}; if [ $err -ne 0 ]; then bg-ping -u "http://gencluster.prib.upf.edu:9090/api/status?id=dd68162522aa&host=172.20.16.27&port=8282&line=STA:Error"; exit $err; fi
bg-ping -u "http://gencluster.prib.upf.edu:9090/api/status?id=dd68162522aa&host=172.20.16.27&port=8282&line=STA:Done"
