#!/usr/bin/env bash

# Create a requirements file
(singularity exec cgi_develop.simg pip3 list --format=columns) | tail -n+3 | grep -v cancergenomeinterpreter | awk '{print($1"=="$2)}' > licensing.txt

# Add license info to this file
wget https://raw.githubusercontent.com/thedataincubator/pylicense/master/pylicense.py
python2 pylicense.py licensing.txt
rm pylicense.py